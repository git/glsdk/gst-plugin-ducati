/* GStreamer
 * Copyright (c) 2017, Texas Instruments Incorporated
 * Copyright (c) 2011, Collabora Ltd.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Alessandro Decina <alessandro.decina@collabora.com>
 *         ti <ti@ti.com>
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "gstducati.h"
#include "gstducatijpegenc.h"

#include <string.h>

#include <math.h>

#define GST_CAT_DEFAULT gst_ducati_debug

enum
{
  LAST_SIGNAL
};

static gboolean gst_ducati_jpegenc_allocate_params (GstDucatiVidEnc *
    self, gint params_sz, gint dynparams_sz, gint status_sz, gint inargs_sz,
    gint outargs_sz);
static gboolean gst_ducati_jpegenc_configure (GstDucatiVidEnc * self);


static GstStaticPadTemplate gst_ducati_jpegenc_sink_template =
GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS (GST_VIDEO_CAPS_MAKE ("NV12"))
    );

static GstStaticPadTemplate gst_ducati_jpegenc_src_template =
GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("image/jpeg, "
        "parsed = (boolean)true, "
        "width = (int)[ 32, 4096 ], "
        "height = (int)[ 32, 4096 ], "
        "framerate = (fraction)[ 0, max ];")
    );

#define parent_class gst_ducati_jpegenc_parent_class
G_DEFINE_TYPE (GstDucatiJPEGEnc, gst_ducati_jpegenc, GST_TYPE_DUCATIVIDENC);

static void
gst_ducati_jpegenc_class_init (GstDucatiJPEGEncClass * klass)
{
  GstDucatiVidEncClass *videnc_class;
  GstElementClass *element_class = GST_ELEMENT_CLASS (klass);

  videnc_class = GST_DUCATIVIDENC_CLASS (klass);

  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&gst_ducati_jpegenc_src_template));
  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&gst_ducati_jpegenc_sink_template));

  gst_element_class_set_metadata (element_class, "JPEG Encoder",
      "Codec/Encoder/Video",
      "Encode raw video into JPEG stream",
      "ti <ti@ti.com>");

  GST_DUCATIVIDENC_CLASS (element_class)->codec_name = "ivahd_jpegvenc";

  videnc_class->allocate_params = gst_ducati_jpegenc_allocate_params;
  videnc_class->configure = gst_ducati_jpegenc_configure;
}

static void
gst_ducati_jpegenc_init (GstDucatiJPEGEnc * self)
{
  GST_DEBUG ("gst_ducati_jpegenc_init");
}

static gboolean
gst_ducati_jpegenc_configure (GstDucatiVidEnc * videnc)
{
  GstDucatiJPEGEnc *self = GST_DUCATIJPEGENC (videnc);
  IJPEGVENC_Params *params;
  IJPEGVENC_DynamicParams *dynParams;
  const GstVideoCodecState *state;
  GstCaps *caps;
  const char *s;
  gboolean ret = TRUE;

  if (!GST_DUCATIVIDENC_CLASS (parent_class)->configure (videnc))
    return FALSE;

  params = (IJPEGVENC_Params *) videnc->params;

  params->maxThumbnailHSizeApp0 = 4096;
  params->maxThumbnailHSizeApp1 = 4096;
  params->maxThumbnailVSizeApp0 = 4096;
  params->maxThumbnailVSizeApp1 = 4096;
  params->debugTraceLevel = 0;

  params->lastNFramesToLog = 0;
  params->Markerposition   = 0;

  params->rateControlParams.VBRDuration     = 8;
  params->rateControlParams.VBRsensitivity  = 0;
  params->rateControlParams.vbvUseLevelThQ5 = 0;

  params->rateControlParams.rateControlParamsPreset = 1;
  params->rateControlParams.rcAlgo = 0;
  params->rateControlParams.qpMaxI = 51;
  params->rateControlParams.qpMinI = 1;
  params->rateControlParams.qpI = -1;
  params->rateControlParams.initialBufferLevel = 10000000;
  params->rateControlParams.HRDBufferSize = 10000000;
  params->rateControlParams.discardSavedBits = 0;

  videnc->params->rateControlPreset = 4;
  videnc->params->maxBitRate = 7500000;
  videnc->params->minBitRate = 4500000;

  dynParams = (IJPEGVENC_DynamicParams *) videnc->dynParams;

  dynParams->restartInterval = 0;
  dynParams->qualityFactor = 50;
  dynParams->quantTable = NULL;

  dynParams->enablePrivacyMasking = 0;

  state = videnc->input_state;
  caps = gst_caps_new_simple ("image/jpeg",
      "parsed", G_TYPE_BOOLEAN, TRUE,
      "width", G_TYPE_INT, videnc->rect.w,
      "height", G_TYPE_INT, videnc->rect.h,
      "framerate", GST_TYPE_FRACTION, GST_VIDEO_INFO_FPS_N (&state->info), GST_VIDEO_INFO_FPS_D (&state->info),
      NULL);
  ret = gst_pad_set_caps (GST_VIDEO_ENCODER_SRC_PAD (self), caps);

  return ret;
}

static gboolean
gst_ducati_jpegenc_allocate_params (GstDucatiVidEnc *
    videnc, gint params_sz, gint dynparams_sz, gint status_sz, gint inargs_sz,
    gint outargs_sz)
{
  return GST_DUCATIVIDENC_CLASS (parent_class)->allocate_params (videnc,
      sizeof (IVIDENC2_Params), sizeof (IVIDENC2_DynamicParams),
      sizeof (IVIDENC2_Status), sizeof (IVIDENC2_InArgs),
      sizeof (IVIDENC2_OutArgs));
}
