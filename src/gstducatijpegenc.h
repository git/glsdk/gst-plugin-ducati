/*
 * GStreamer
 * Copyright (c) 2017, Texas Instruments Incorporated
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef __GST_DUCATIJPEGENC_H__
#define __GST_DUCATIJPEGENC_H__

#include <ti/sdo/codecs/jpegvenc/ijpegenc.h>
#include "gstducatividenc.h"

#define GST_TYPE_DUCATIJPEGENC \
  (gst_ducati_jpegenc_get_type())
#define GST_DUCATIJPEGENC(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_DUCATIJPEGENC,GstDucatiJPEGEnc))
#define GST_DUCATIJPEGENC_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_DUCATIJPEGENC,GstDucatiJPEGEncClass))
#define GST_IS_DUCATIJPEGENC(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_DUCATIJPEGENC))
#define GST_IS_DUCATIJPEGENC_CLASS(obj) \
  (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_DUCATIJPEGENC))
#define GST_DUCATIJPEGENC_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS((obj), GST_TYPE_DUCATIJPEGENC, GstDucatiJPEGEncClass))

typedef struct _GstDucatiJPEGEnc GstDucatiJPEGEnc;
typedef struct _GstDucatiJPEGEncClass GstDucatiJPEGEncClass;

struct _GstDucatiJPEGEnc
{
  GstDucatiVidEnc parent;
};

struct _GstDucatiJPEGEncClass
{
  GstDucatiVidEncClass parent_class;
};

GType gst_ducati_jpegenc_get_type (void);

#endif
